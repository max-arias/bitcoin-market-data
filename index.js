
(function($){
  var urlsToScrape = [
    'http://coinmarketcap.com/assets/golem-network-tokens/#BTC',
    'http://coinmarketcap.com/assets/gnt/#BTC',
    'http://coinmarketcap.com/assets/basic-attention-token/#BTC',
    'http://coinmarketcap.com/assets/edgeless/#BTC',
    'http://coinmarketcap.com/assets/waves/#BTC',
    'http://coinmarketcap.com/assets/status/#BTC',
    'http://coinmarketcap.com/assets/storj/#BTC',
    'http://coinmarketcap.com/assets/guppy/#BTC',
    'http://coinmarketcap.com/assets/monaco/#BTC',
    'http://coinmarketcap.com/assets/singulardtv/#BTC',
    'http://coinmarketcap.com/assets/cofound-it/#BTC',
    'http://coinmarketcap.com/assets/trust/#BTC',
    'http://coinmarketcap.com/assets/wings/#BTC',
    'http://coinmarketcap.com/assets/tokencard/#BTC',
    'http://coinmarketcap.com/assets/patientory/#BTC',
    'http://coinmarketcap.com/assets/humaniq/#BTC',
    'http://coinmarketcap.com/assets/aragon/#BTC',
    'http://coinmarketcap.com/assets/mysterium/#BTC',
    'http://coinmarketcap.com/assets/lunyr/#BTC',
    'http://coinmarketcap.com/assets/quantum-resistant-ledger/#BTC',
    'http://coinmarketcap.com/assets/melon/#BTC',
    'http://coinmarketcap.com/assets/gnosis-gno/#BTC',
    'http://coinmarketcap.com/assets/bancor/#BTC',
    'http://coinmarketcap.com/assets/angur/#BTC',
    'http://coinmarketcap.com/assets/firstblood/#BTC'
  ];

  $.ajaxPrefilter( function (options) {
    if (options.crossDomain && jQuery.support.cors) {
      var http = (window.location.protocol === 'http:' ? 'http:' : 'https:');
      options.url = http + '//cors-anywhere.herokuapp.com/' + options.url;
      //options.url = 'http://cors.corsproxy.io/url=' + options.url;
    }
  });

  $.each(urlsToScrape, function(index, urlToStrape) {
    $.get(urlToStrape, function(data) {
      var dataTable = $(data).find('#markets #markets-table tbody tr');
      $('#table tbody').append(dataTable);
    });
  });

  $(document).ajaxStop(function () {
    $('.loader').hide();
    $('#table').removeClass('hidden');

    $('#table tbody tr').each(function(index) {
      $(this).removeClass();
      $(this).find('td:first').text(index + 1);
    });

    $('#table').DataTable({
      'paging': false
    });
  });

})(jQuery);